const ANDROID_VERSION_APP = '1.0.2';
const ANDROID_VERSION_NUMBER = 3;
const IOS_VERSION_APP = '1.0.2';
const IOS_VERSION_NUMBER = 3;
const SECRET_KEY = 'dev';
const IAP_SECRET_KEY = 'cJg4NJMCcJg4NJMC';
const SMART_LOOK_API_KEY = 'dc6600fbfdd2086be103b821071b297417d5b2e7';

const LIGHT_THEME = 'light_theme';
const DARK_THEME = 'dark_theme';
const VELVET_THEME = 'velvet_theme';

const BASE_WIDTH = 430.0;
const MAX_WIDTH = 900.0;
const int DELAY_TIME = 1000;

const HT_TOKEN = 'HTT';

const IAP_ITEM_100 = 'com.mintty.marketplace.100';
const IAP_PRODUCT_LIST = [IAP_ITEM_100];

const DEFAULT_LANG = 'vi';

const LANG_ENGLISH = 'en';
const LANG_GERMAN = 'gm';
const LANG_KOREAN = 'kr';
const LANG_JAPANESE = 'jp';
const LANG_CHINESE = 'cn';
const LANG_SPANISH = 'sp';
const LANG_FRENCH = 'fr';
const LANG_RUSSIAN = 'ru';
const LANG_VIETNAMESE = 'vi';
const LANG_TURKISH = 'tu';

const List<String> LIST_LANGUAGE = [
  LANG_ENGLISH,
//  LANG_GERMAN,
//  LANG_KOREAN,
//  LANG_JAPANESE,
//  LANG_CHINESE,
//  LANG_SPANISH,
//  LANG_FRENCH,
//  LANG_RUSSIAN,
  LANG_VIETNAMESE,
//  LANG_TURKISH
];

const String BORDER_LEFT = 'left';
const String BORDER_RIGHT = 'right';

const String PERMISSION_GRANTED = 'granted';
const String PERMISSION_DENIED = 'denied';
const String PERMISSION_UNKNOWN = 'unknown';

const String TRUE = 'true';
const String FALSE = 'false';

const int RESEND_OTP = 60;

const double DEFAULT_ROYALTIES = 10.0;
const double MIN_ROYALTIES = 0.0;
const double MAX_ROYALTIES = 50.0;

const String AUTHENTICATE_PROCESSING = 'AUTHENTICATE_PROCESSING';
const String AUTHENTICATE_DONE = 'AUTHENTICATE_DONE';
const String AUTHENTICATE_LOCK = 'AUTHENTICATE_LOCK';

const String IDENTITY_CARD_FRONT = 'IDENTITY_CARD_FRONT';
const String IDENTITY_CARD_BACK = 'IDENTITY_CARD_BACK';
const String PASSPORT = 'PASSPORT';
const String PORTRAIT = 'PORTRAIT';
const String SIGNATURE = 'SIGNATURE';

const String TYPE_IDENTITY_CARD = 'identification';
const String TYPE_PASSPORT = 'passport';

const String API_ANDROID_KEY = 'AIzaSyCfFtIsKFfRT4133-3oBUTLGA7Jfb5zmb4';
const String API_IOS_KEY = 'AIzaSyCfFtIsKFfRT4133-3oBUTLGA7Jfb5zmb4';

const String GENDER_MALE = 'male';
const String GENDER_FEMALE = 'female';

const String KYC_STATUS_REQUEST = 'request';
const String KYC_STATUS_DONE = 'done';
const String KYC_STATUS_NONE = 'none';
const String CREATOR_STATUS_NONE = 'none';
const String CREATOR_STATUS_REQUEST = 'request';
const String CREATOR_STATUS_DONE = 'done';

const String STATUS_FEE_CHECKED = 'fee_checked';
const String STATUS_PRE_PAID = 'pre_paid';
const String STATUS_RETRYING = 'retrying';
const String STATUS_PENDING = 'pending';
const String STATUS_PROCESSING = 'processing';
const String STATUS_PREPARED = 'prepared';
const String STATUS_SUCCESS = 'success';
const String STATUS_DONE = 'done';
const String STATUS_CANCEL = 'cancel';
const String STATUS_USER_CONFIRMED = 'user_confirmed';
const String STATUS_REQUEST = 'request';
const String STATUS_NONE = 'none';
const String STATUS_FAILED = 'failed';
const String STATUS_OPEN = 'open';
const String STATUS_OPENING = 'opening';
const String STATUS_CONFIRMING = 'confirming';
const String STATUS_CANCELED = 'cancelled';
const String STATUS_ON_HOLD = 'on_hold';
const String STATUS_PRE_SUCCESS = 'pre_success';

const String TYPE_CREATE = 'create';
const String TYPE_BUY = 'buy';
const String TYPE_MINT = 'mint';
const String TYPE_WITHDRAW = 'withdraw';
const String TYPE_TRANSFER = 'transfer';

const String TYPE_IN = 'in';
const String TYPE_OUT = 'out';

const String WALLET_CODE_VND = 'VND';
const String WALLET_SHORT_CODE_VND = 'đ';
const String WALLET_CODE_USD = 'USD';
const String WALLET_CODE_TOKEN = 'HTT';

const int FRACTION_DIGIT_TOKEN = 6;

const String ACCOUNT_MODE_REAL = 'real';
const String ACCOUNT_MODE_VIRTUAL = 'virtual';

const String RATE_APP_IOS =
    'https://apps.apple.com/app/id1613902050?action=write-review';
const String RATE_APP_ANDROID =
    'https://play.google.com/store/apps/details?id=com.mintty.nft';
const String APP_STORE_ID = '1613902050';

const String DEFAULT_FCM_TOPIC = 'fcm_all';
const String NOTIFICATION_TYPE_USER_WITHDRAW = 'USER_WITHDRAW';
const String NOTIFICATION_TYPE_USER_KYC = 'USER_KYC';
const String NOTIFICATION_TYPE_USER_TRANSFER = 'USER_TRANSFER';

const String IS_MAINTAIN = 'is_maintain';
const String EXPECTED_TIME = 'expected_time';
const String CF_ANDROID_VERSION_APP = 'android_version_app';
const String CF_ANDROID_VERSION_NUMBER = 'android_version_number';
const String CF_ANDROID_FORCE_UPDATE = 'android_force_update';
const String CF_IOS_VERSION_APP = 'ios_version_app';
const String CF_IOS_VERSION_NUMBER = 'ios_version_number';
const String CF_IOS_FORCE_UPDATE = 'ios_force_update';
const String CF_MAINTAIN_DEFAULT = 'maintain-staging';
const String CF_MAINTAIN = 'maintain';
const String CF_VERSION = 'version';
const String CF_NOTIFICATIONS_DEFAULT = 'notifications-staging';
const String CF_NEW_NOTIFICATION = 'new_notification';
const String CF_BUY_HISTORIES = 'buy_histories';
const String CF_MINT_NFT_FEE = 'mint_nft_fee';
const String CF_MINT_COLLECTION_FEE = 'mint_collection_fee';
const String CF_CONFIRM_BID = 'confirm_bid';
const String CF_CONFIRM_BUY = 'confirm_buy';
const String CF_DONE_AUCTION = 'done_auction';
const String CF_CLOSE_AUCTION = 'close_auction';
const String CF_MINT_UNIQUE_ID = 'update_unique_id';
const String CF_KYC_STATUS = 'kycStatus';
const String CF_REQUEST_CREATOR = 'request_creator';
const String CF_KYC = 'kyc';
const String CF_CONFIRM_DEPOSIT = 'confirm_deposit';
const String CF_USER_CONFIRM_DEPOSIT = 'user_confirm_deposit';
const String CF_CANCEL_DEPOSIT = 'cancel_deposit';
const String CF_CONFIRM_WITHDRAW = 'confirm_withdraw';
const String CF_CANCEL_WITHDRAW = 'cancel_withdraw';
const String CF_UPDATE_WALLET = 'update_wallet';
const String CF_TYPE = 'type';
const String CF_DATA = 'data';
const String CF_LAST_UPDATE_TIME = 'lastUpdatedTime';

const String STATUS_TYPE_DEFAULT = 'default';
const String STATUS_TYPE_ADVANCE = 'advance';

const String CREATED_BY_SYSTEM = 'system';
const String CREATED_BY_USER = 'user';

const String TOPIC_ALL = 'fcm_all';

const String NOTIFICATION_KEY = 'notification_key';
const String NOTIFICATION_TITLE = 'title';
const String NOTIFICATION_BODY = 'body';
const String NOTIFICATION_ID = 'notification_id';
const String NOTIFICATION_OBJECT_ID = 'object_id';
const String NOTIFICATION_TYPE = 'type';
const String NOTIFICATION_SLUG = 'slug';

const String NOTIFICATION_TYPE_BLOG = 'article';
const String NOTIFICATION_TYPE_FORECAST = 'forecast_new';
const String NOTIFICATION_TYPE_DEFAULT = 'notification';

const String DEMAND_PROFIT_IN = 'in';
const String DEMAND_PROFIT_OUT = 'out';

const String TYPE_SELLING = 'sale';
const String TYPE_BID = 'auction';

const String STATUS_OFFER_STATUS_OPENING = 'opening';
const String STATUS_OFFER_CONFIRMING = 'confirming';
const String STATUS_OFFER_CANCELED = 'canceled';
const String STATUS_OFFER_DONE = 'done';

const String CREATOR_7_DAY = 'creator7dValuation';
const String CREATOR_30_DAY = 'creator30dValuation';
const String CREATOR_90_DAY = 'creator90dValuation';
const String COLLECTOR_7_DAY = 'collector7dValuation';
const String COLLECTOR_30_DAY = 'collector30dValuation';
const String COLLECTOR_90_DAY = 'collector90dValuation';

const String SORT_TRENDING_NFT = 'trending_nft';

const String SORT_UPDATED_OWNER_AT = 'updatedOwnerAt';
const String SORT_UPDATED_AT = 'updatedAt';
const String SORT_VIEW_COUNT = 'viewsCount';
const String SORT_LIKE_COUNT = 'likesCount';
const String SORT_TYPE_DESC = 'desc';
const String SORT_TYPE_ASC = 'asc';
const String ENABLE_NFT_LAUNCHPAD = '1';
const String DISABLE_NFT_LAUNCHPAD = '0';

const String SORT_BY_NEWEST = 'new_to_old';
const String SORT_BY_OLDEST = 'old_to_new';
const String SORT_BY_LOW_TO_HIGH = 'low_to_high';
const String SORT_BY_HIGH_TO_LOW = 'high_to_low';
const String SORT_BY_ON_SALE = 'just_posted_for_sale';
const String SORT_BY_PRICE = 'sortPrice';
const String SORT_BY_UPDATE_PRICE = 'updatedPriceAt';

const String PARAM_LOW_TO_HIGH = '&on_sale=true&sort_by=price&&sort_type=asc';
const String PARAM_HIGH_TO_LOW = '&on_sale=true&sort_by=price&&sort_type=desc';
const String PARAM_NEWEST = 'on_sale=true';

const String ALL = 'All';
const String ON_SALE = 'on_sale';

const String PARAM_LOW_TO_HIGH_COLLECTION = '&sort_by=price&&sort_type=asc';
const String PARAM_HIGH_TO_LOW_COLLECTION = 'sort_by=price&&sort_type=desc';

const String SORT_VOL_1D = 'vol1d';
const String SORT_VOL_2D = 'vol2d';
const String SORT_VOL_7D = 'vol7d';
const String SORT_VOL_14D = 'vol14d';
const String SORT_VOL_30D = 'vol30d';
const String SORT_VOL_60D = 'vol60d';
const String SORT_VOL_90D = 'vol90d';
const String SORT_CREATED_AT = 'createdAt';
const String WEEK_SCORE = 'weekScore';
const String MONTH_SCORE = 'monthScore';
const String QUARTER_SCORE = 'quarterScore';
const String COLLECTION_DEFAULT = 'default';

const List<String> SHORT_NFT = [
  ALL,
  SORT_BY_LOW_TO_HIGH,
  SORT_BY_HIGH_TO_LOW,
  SORT_BY_ON_SALE,
  SORT_VIEW_COUNT,
  SORT_LIKE_COUNT,
  SORT_BY_NEWEST,
  SORT_BY_OLDEST
];

const List<String> FILTER_COLLECTION = [
  SORT_VOL_7D,
  SORT_VOL_30D,
  SORT_VOL_90D,
];

const String FILTER_BY_OWNER = 'owning';
const String FILTER_BY_SELLING = 'selling';
const String FILTER_BY_CREATOR = 'created';
const String FILTER_BY_LIST_LIKE = 'liked';
const String FILTER_ALL = 'all';
const String FILTER_MINTED = 'minted';
const String FILTER_NOT_MINTED = 'not_minted';
const String FILTER_ON_SALE = 'on_sale';
const String FILTER_NOT_ON_SALE = 'not_on_sale';
const String FILTER_MOST_VIEWED = 'most_viewed';
const String FILTER_MOST_FAVORITE = 'most_favorite';

const String CATEGORY_BID = 'bid';
const String CATEGORY_MINT_COLLECTION = 'mint_collection';
const String CATEGORY_MINT_NFT = 'mint';
const String CATEGORY_WITHDRAW_NFT = 'withdraw';
const String CATEGORY_WITHDRAW_NFT_HOLDING = 'withdraw_nft';
const String CATEGORY_WITHDRAW = 'withdraw';

const String CATEGORY_TRANSFER = 'transfer';
const String CATEGORY_LISTING = 'listing';
const String CATEGORY_LIKES = 'like';
const String CATEGORY_SALE = 'sale';
const String CATEGORY_PURCHASE = 'purchase';
const String CATEGORY_BIDS = 'bids';
const String CATEGORY_RECEIVED = 'received';

const List<String> FILTER_NFT = [
  FILTER_BY_OWNER,
  FILTER_BY_SELLING,
  FILTER_BY_CREATOR,
  FILTER_BY_LIST_LIKE
];
