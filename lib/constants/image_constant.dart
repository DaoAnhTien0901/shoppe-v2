class ImageConstant {
  static String noData = 'assets/animations/no_data_animaiton.json';
  static String icNoEye = 'assets/3.0x/icon_no_eye.png';
  static String icCboxFalse = 'assets/3.0x/icon_checkbox_false.png';
  static String icGoogle = 'assets/3.0x/icon_google.png';
  static String icFacebook = 'assets/3.0x/icon_facebook.png';
  static String icApple = 'assets/3.0x/icon_apple.png';
  static String icCboxTrue = 'assets/3.0x/icon_checkbox_true.png';
  static String icLogSuccess = 'assets/3.0x/icon_login_success.png';
  static String icCreatPw = 'assets/3.0x/icon_createpassword.png';
  static String icProfile = 'assets/3.0x/icon_profile.png';
  static String icChatgpt = 'assets/3.0x/icon_chatgpt.png';
  static String icChatAi = 'assets/ChatAi.png';
  static String icAiAssitants = 'assets/AiAssistants.png';
  static String icAccount = 'assets/Account.png';
  static String logoChatGPT = 'assets/logo_chat_gpt.png';
  static String logoUpdatePro = 'assets/upgrade_to_pro.png';
  static String nextWhile = 'assets/next_while.png';
  static String security = 'assets/Shield Done.png';
  static String profile = 'assets/Profile.png';
  static String paper = 'assets/Paper.png';
  static String logOut = 'assets/Logout.png';
  static String lock = 'assets/Lock.png';
  static String infoSquare = 'assets/Info Square.png';
  static String document = 'assets/Document.png';

  static String next = 'assets/next.png';
  static String logo = 'assets/logo.png';
  static String uploadPhoto = 'assets/UploadPhoto.png';
  static String backgroudLogin = 'assets/backgroud_login.png';
  static String backgroudPassword = 'assets/backgroud_password.png';
  static String representativeImage = 'assets/representative_image.png';
  static String backgroudCreateAccount =
      'assets/3.0x/backgroud_create_account.png';
  static String ellipse = 'assets/ellipse.png';
}
