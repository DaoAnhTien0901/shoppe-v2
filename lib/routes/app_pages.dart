import 'package:shoppe_flutter/page/create_password/create_password.dart';
import 'package:shoppe_flutter/page/home/home.dart';
import 'package:shoppe_flutter/page/login/login_binding.dart';
import 'package:shoppe_flutter/page/login/login_page.dart';
import 'package:shoppe_flutter/page/password/password_binding.dart';
import 'package:shoppe_flutter/page/password/password_page.dart';
import 'package:shoppe_flutter/page/register/register.dart';
import 'package:shoppe_flutter/page/start/start_binding.dart';
import 'package:shoppe_flutter/page/start/start_page.dart';
import 'package:shoppe_flutter/page/verification/verification.dart';
import 'package:shoppe_flutter/page/resetpassword/resetpassword.dart';
import 'package:shoppe_flutter/page/profile/profile.dart';
import 'package:shoppe_flutter/page/welcom_screen/welcome_page.dart';
import 'package:shoppe_flutter/page/welcom_screen/welcomescreen.dart';
import 'package:get/get.dart';

import '../page/create_account/create_acccount_binding.dart';
import '../page/create_account/create_account_page.dart';
part 'app_routes.dart';

class AppPages {
  static const INITIAL = AppRoutes.start;

  static final routes = [
    GetPage(
      name: AppRoutes.home,
      page: () => const HomePage(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: AppRoutes.verification,
      page: () => const VerificationPage(),
      binding: VerificationBinding(),
    ),
    GetPage(
      name: AppRoutes.create_password,
      page: () => const CreatepasswordPage(),
      binding: CreatepasswordBinding(),
    ),
    GetPage(
      name: AppRoutes.resetpassword,
      page: () => const ResetpasswordPage(),
      binding: ResetpasswordBinding(),
    ),
    GetPage(
      name: AppRoutes.profile,
      page: () => ProfilePage(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: AppRoutes.register,
      page: () => const RegisterPage(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: AppRoutes.welcom_screen,
      page: () => const WelcomePage(),
      binding: WelcomeBinding(),
    ),
    GetPage(
      name: AppRoutes.start,
      page: () => const StartPage(),
      binding: StartBinding(),
    ),
    GetPage(
      name: AppRoutes.create_account,
      page: () => const CreateAccountPage(),
      binding: CreateAccountBinding(),
    ),
    GetPage(
      name: AppRoutes.login,
      page: () => const LoginPage(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: AppRoutes.password,
      page: () => const PasswordPage(),
      binding: PasswordBinding(),
    ),
  ];
}
