part of 'app_pages.dart';

abstract class AppRoutes {
  static const splash = '/';
  static const auth = '/auth';
  static const register = '/register';
  static const login = '/LOGIN';
  static const home = '/home';
  static const resetpassword = '/resetpassword';
  static const create_password = '/create_password';
  static const verification = '/verification';
  static const profile = '/profile';
  static const welcom_screen = '/welcom_screen';
  static const start = '/start';
  static const create_account = '/create_account';
  static const password = '/password';
}
