import 'package:get/get.dart';
import 'package:shoppe_flutter/page/password/password_controller.dart';

class PasswordBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PasswordController());
  }
}
