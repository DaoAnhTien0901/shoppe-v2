import 'package:shoppe_flutter/constants/image_constant.dart';
import 'package:shoppe_flutter/helpers/extension/responsive.dart';
import 'package:shoppe_flutter/routes/app_pages.dart';
import 'package:shoppe_flutter/utils/screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shoppe_flutter/widget/button_with_icon.dart';
import '../../style/app_style.dart';
import 'password_controller.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class PasswordPage extends GetView<PasswordController> {
  const PasswordPage({super.key});

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 430, allowFontScaling: true)
      ..init(context);
    return Scaffold(
        backgroundColor: CupertinoColors.white,
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Image.asset(ImageConstant.backgroudPassword),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 149.h,
                  ),
                  Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 7.w, horizontal: 7.w),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100.w),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 5,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Image.asset(ImageConstant.representativeImage,
                          width: 91.w, height: 91.h)),
                  SizedBox(
                    height: 28.h,
                  ),
                  const Text(
                    'Hello, Romina!!',
                    style: TextStyle(fontWeight: FontWeight.w700, fontSize: 28),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 30.h,
                  ),
                  const Text(
                    'Type your password',
                    style: TextStyle(fontWeight: FontWeight.w300, fontSize: 19),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 23.h,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 81.w),
                    child: PinCodeTextField(
                      appContext: context,
                      length: 4,
                      cursorHeight: 19,
                      enableActiveFill: true,
                      pinTheme: PinTheme(
                          shape: PinCodeFieldShape.box,
                          inactiveColor: Color(0xffF8F8F8),
                          inactiveFillColor: Color(0xffF8F8F8),
                          fieldWidth: 50,
                          borderWidth: 1,
                          borderRadius: BorderRadius.circular(8)),
                    ),
                  ),
                  SizedBox(
                    height: 252.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Get.toNamed(AppRoutes.login);
                        },
                        child: const Center(
                          child: Text(
                            'Not you?',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.w300),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 16.w,
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Container(
                          width: 30.w,
                          height: 30.h,
                          decoration: BoxDecoration(
                              color: Style.primaryColor,
                              borderRadius: BorderRadius.circular(100.sp)),
                          child: const Icon(
                            Icons.arrow_forward_outlined,
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}
