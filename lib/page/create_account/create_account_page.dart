import 'package:shoppe_flutter/constants/image_constant.dart';
import 'package:shoppe_flutter/helpers/extension/responsive.dart';
import 'package:shoppe_flutter/routes/app_pages.dart';
import 'package:shoppe_flutter/style/app_style.dart';
import 'package:shoppe_flutter/utils/screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:shoppe_flutter/widget/button_with_icon.dart';
import 'create_account_controller.dart';

class CreateAccountPage extends GetView<CreateAccountController> {
  const CreateAccountPage({super.key});

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 430, allowFontScaling: true)
      ..init(context);
    return Scaffold(
        backgroundColor: CupertinoColors.white,
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Image.asset(ImageConstant.backgroudCreateAccount),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 30.w, top: 122.h),
                    child: const Text(
                      'Create\nAccount',
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 50,
                          fontFamily: 'Raleway-Bold'),
                    ),
                  ),
                  SizedBox(
                    height: 54.h,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 30.w),
                    child: Image.asset(
                      ImageConstant.uploadPhoto,
                      width: 90.w,
                      height: 90.h,
                    ),
                  ),
                  SizedBox(
                    height: 32.h,
                  ),
                  Container(
                    height: 62.h,
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    child: Align(
                      alignment: Alignment.center,
                      child: TextFormField(
                        style: Style().h5Bold,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          filled:
                              true, // Đặt filled là true để sử dụng fillColor
                          fillColor: const Color(0xffF8F8F8),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(59.29),
                            borderSide: const BorderSide(
                              color: Colors
                                  .transparent, // Màu của border khi nó không được focus
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(59.29),
                            borderSide: const BorderSide(
                              color: Colors
                                  .transparent, // Màu của border khi nó không được focus
                            ),
                          ),
                          hintText: 'Email',
                          hintStyle: const TextStyle(
                              color: Style.greyscale20,
                              fontSize: 13,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  Container(
                    height: 62.h,
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    child: Align(
                      alignment: Alignment.center,
                      child: TextFormField(
                        style: Style().h5Bold,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          filled:
                              true, // Đặt filled là true để sử dụng fillColor
                          fillColor: const Color(0xffF8F8F8),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(59.29),
                            borderSide: const BorderSide(
                              color: Colors
                                  .transparent, // Màu của border khi nó không được focus
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(59.29),
                            borderSide: const BorderSide(
                              color: Colors
                                  .transparent, // Màu của border khi nó không được focus
                            ),
                          ),
                          hintText: 'Password',
                          hintStyle: const TextStyle(
                              color: Style.greyscale20,
                              fontSize: 13,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    child: IntlPhoneField(
                      decoration: InputDecoration(
                        filled: true, // Đặt filled là true để sử dụng fillColor
                        fillColor: const Color(0xffF8F8F8),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(59.29),
                          borderSide: const BorderSide(
                            color: Colors
                                .transparent, // Màu của border khi nó không được focus
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(59.29),
                          borderSide: const BorderSide(
                            color: Colors
                                .transparent, // Màu của border khi nó không được focus
                          ),
                        ),
                        hintText: 'Phone Number',
                        hintStyle: const TextStyle(
                            color: Style.greyscale20,
                            fontSize: 13,
                            fontWeight: FontWeight.w500),
                      ),
                      initialCountryCode: 'IN',
                      onChanged: (phone) {
                        print(phone.completeNumber);
                      },
                    ),
                  ),
                  SizedBox(
                    height: 52.h,
                  ),
                  RadiusButton(
                    text: 'Done'.tr,
                    isFullWidth: true,
                    maxWidth: double.maxFinite,
                    textColor: const Color(0xffF3F3F3),
                    outsidePadding: EdgeInsets.symmetric(horizontal: 20.w),
                    onTap: () {
                      Get.toNamed(AppRoutes.login);
                    },
                  ),
                  SizedBox(
                    height: 24.h,
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.toNamed(AppRoutes.start);
                    },
                    child: const Center(
                      child: Text(
                        'Cancel',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w300),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 53.h,
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}
