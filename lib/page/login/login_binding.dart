import 'package:get/get.dart';
import 'package:shoppe_flutter/page/login/login_controller.dart';

class LoginBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController());
  }
}
