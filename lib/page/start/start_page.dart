import 'package:shoppe_flutter/constants/image_constant.dart';
import 'package:shoppe_flutter/helpers/extension/responsive.dart';
import 'package:shoppe_flutter/routes/app_pages.dart';
import 'package:shoppe_flutter/style/app_style.dart';
import 'package:shoppe_flutter/utils/screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shoppe_flutter/widget/_cached_image.dart';
import 'package:shoppe_flutter/widget/button_with_icon.dart';
import 'start_controller.dart';

class StartPage extends GetView<StartController> {
  const StartPage({super.key});

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 430, allowFontScaling: true)
      ..init(context);
    return Scaffold(
        backgroundColor: CupertinoColors.white,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 232.h,
            ),
            Container(
                padding: EdgeInsets.symmetric(vertical: 21.w, horizontal: 26.w),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(100.w),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child:
                    Image.asset(ImageConstant.logo, width: 82.w, height: 92.h)),
            SizedBox(height: 24.h),
            Text(
              "Shoppe",
              style: Style().bodyLargeBold.copyWith(fontSize: 52.sp),
            ),
            SizedBox(
              height: 18.h,
            ),
            Text("Beautiful eCommerce UI Kit \n for your online store",
                textAlign: TextAlign.center,
                style: Style().bodyLarge.copyWith(fontSize: 19.sp)),
            SizedBox(
              height: 106.w,
            ),
            RadiusButton(
              text: 'Let is get started'.tr,
              fontSize: 22,
              isFullWidth: true,
              maxWidth: double.maxFinite,
              textColor: Colors.white,
              backgroundColor: Style.primaryColor,
              outsidePadding: EdgeInsets.symmetric(horizontal: 20.w),
              onTap: () {
                Get.toNamed(AppRoutes.create_account);
              },
            ),
            SizedBox(
              height: 24.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("I already have an account",
                    style: Style().bodyLarge.copyWith(fontSize: 15.sp)),
                SizedBox(
                  width: 16.w,
                ),
                Container(
                  padding: EdgeInsets.all(10.w),
                  decoration: BoxDecoration(
                      color: Style.primaryColor,
                      borderRadius: BorderRadius.circular(100.sp)),
                  child: const Icon(
                    Icons.arrow_forward_outlined,
                    color: Colors.white,
                  ),
                )
              ],
            )
          ],
        ));
  }
}
