import 'package:shoppe_flutter/constants/image_constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StartController extends GetxController {
  final indexPage = 0.obs;
  final PageController pageController = PageController();
  final List<String> pages = [
    ImageConstant.icChatAi,
    ImageConstant.icAiAssitants,
    ImageConstant.icAccount,
  ].obs;

  final List<String> title = [
    "The best AI Chatbot app in this century",
    "Various AI Assistants to help you more",
    "Try premium for your unlimited usage",
  ];
}
