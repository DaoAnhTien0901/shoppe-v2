import 'package:get/get.dart';

import 'start_controller.dart';

class StartBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => StartController());
  }
}
